% Build instructions
% Sat, Jan 11, 2020
% Built by [a lobster](https://lobste.rs/u/btbytes)

## Dependencies

* [pandoc](https://pandoc.org)
* [pup](https://github.com/ericchiang/pup)
* `bash`

## Source Code

See the [bitbucket repository: btbytes/lobstersweb](https://bitbucket.org/btbytes/lobstersweb/src/master/)

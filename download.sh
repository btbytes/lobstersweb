#!/bin/bash
set -x

cat <<EOF > index.md
% Lobste.rs web
% Fri, Jan 10, 2020
% [A lobster](https://lobste.rs/u/btbytes)

A directory of [lobste.rs](https://lobste.rs) users.

EOF


while read p; do
    url=`echo $p | awk '{printf $1}'`
    title=`echo $p | awk '{$1=""; print $0;}'`
    frag=`echo $url | awk -F/ '{print $5}'`
    mdfile="${frag}.md"
    html="${frag}.html"
    dt=`date "+%b, %d %Y"`
    printf "* [%s](%s)\n" "${title}" "${html}" >> index.md
    cat <<EOF > "$mdfile" 
% $title
% 
% [source]($url)

A directory of [lobste.rs](https://lobste.rs) users websites as provided by them in [this thread]($url).

EOF
    
    curl -s $url | pup "a attr{href}" | grep ^http | grep -v ^https://lobste.rs | awk -F/ 'BEGIN { OFS="//";} {print $1,$3}' | sort | uniq | xargs -I{} echo "- <{}>" >> "$mdfile"
done < "urls.txt"


SOURCES=$(wildcard *.md)
OBJS = $(patsubst %.md, %.html, $(SOURCES))

all: $(OBJS)

download:  urls.txt download.sh
	./download.sh

%.html: %.md Makefile
	pandoc $< -o $@ --css=https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/light.min.css --standalone

publish:
	now --prod
